package com.ilhanli.omer.deviceshakesample;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final int DEFUALT_MS = 100;

    private SensorManager mSensorManager;

    private Sensor mAccelerometer;

    private ShakeDetector mShakeDetector;

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.text_view);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        mShakeDetector = new ShakeDetector();

        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {

            @Override
            public void onShake(int count) {

                textView.setText("Shake Number --- count : " + count);

                vibrate();
            }
        });
    }


    public void vibrate() {

        Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        if (vibrator.hasVibrator()) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                vibrator.vibrate(VibrationEffect.createOneShot(DEFUALT_MS, VibrationEffect.DEFAULT_AMPLITUDE));

            } else {

                //deprecate API 26
                vibrator.vibrate(DEFUALT_MS);
            }
        }
    }

    @Override
    public void onResume() {

        super.onResume();

        mSensorManager.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {

        super.onPause();

        mSensorManager.unregisterListener(mShakeDetector);
    }


}
